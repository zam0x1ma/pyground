'''
The final activity for the Advanced Python section is a drive-wide FTP-like
tool. You should be able to receive multiple connections, each on their 
own thread. You should take several commands:
DRIVESEARCH <filename>
    DRIVESEARCH looks for the given filename across the entire drive. If
    it finds the file, it sends back the drive location.
DIRSEARCH <directory> <filename>
    DIRSEARCH looks for the file in the given directory or its 
    subdirectories. If it finds the file, it sends back the location.
DOWNLOAD <filename>
    DOWNLOAD requires the full file path, or at least the relative path,
    of the file. It sends the contents of the file across the network.
UPLOAD <filename>
    UPLOAD requires the full file path, or at least the relative path,
    where the user wants to locate the file. It reads the file contents
    from across the network connection.
CLOSE
    CLOSE ends the connection
    
This activity will require you to use multithreading, ctypes, regular
expressions, and some libraries with which you're unfamiliar. ENJOY!
'''

import os, re, socket, threading, struct, fnmatch
from ctypes import *

cdll.LoadLibrary('libc.so.6')
libc = CDLL('libc.so.6')

#64-bit machine
libc.fopen.restype = c_long
libc.fread.argtypes = [c_void_p, c_size_t, c_size_t, c_long]
libc.fwrite.argtypes = [c_void_p, c_size_t, c_size_t, c_long]
libc.fclose.argtypes = [c_long]

'''
#32-bit machine
libc.fopen.restype = c_int
libc.fread.argtypes = [c_void_p, c_size_t, c_size_t, c_int]
libc.fwrite.argtypes = [c_void_p, c_size_t, c_size_t, c_int]
libc.fclose.argtypes = [c_int]
'''

def read_file(file_name): #ctypes
    try:
        c_file = libc.fopen(file_name, 'r+')
        buf = create_string_buffer(1024)
        libc.fread(buf, 1, 4096, c_file)
        ret_value = buf.value
    except:
        ret_value = -1
    finally:
        libc.fclose(c_file)
    return ret_value

def create_file(file_name, data): #ctypes
    try:
        c_file = libc.fopen(file_name, 'w+')
        buf = create_string_buffer(data)
        libc.fwrite(buf, 1, len(data), c_file)
        ret_value = 0
    except:
        ret_value = -1
    finally:
        libc.fclose(c_file)
    return ret_value

def recv_data(sock): #Implement a networking protocol
    data_len, = struct.unpack('!I', sock.recv(4))
    return sock.recv(data_len)

def send_data(sock, data): #Implement a networking protocol
    data_len = len(data)
    sock.send(struct.pack('!I', data_len))
    sock.send(data)
    return

def search(file_name_pattern, path):
    result = []
    for dir_path, dir_names, file_names in os.walk(path):
        for fname in file_names:
            if fnmatch.fnmatch(fname, file_name_pattern):
                result.append(os.path.join(dir_path, fname))
    if result:
        return result[0]
    return -1

def search_drive(file_name_pattern): #DRIVESEARCH
    return search(file_name_pattern, '/')

def search_directory(file_name_pattern): #DIRSEARCH
    return search(file_name_pattern, os.getcwd())

def send_file_contents(file_name, usersock, userinfo): #DOWNLOAD
    read_file_result = read_file(file_name) 
    if (read_file_result == -1):
        send_data(usersock, 'File read failed')
    else:
        send_data(usersock, read_file_result)
    return

def receive_file_contents(file_name, usersock): #UPLOAD
    file_data = recv_data(usersock)
    if (create_file(file_name, file_data) == -1):
        send_data(usersock, 'File creation failed')
    return

def handle_connection(usersock, userinfo):
    continue_sentinel = True

    while (continue_sentinel):
        send_data(usersock, 'COMMAND_REQUEST')
        option = recv_data(usersock).upper()

        if (option == 'DRIVESEARCH'):
            send_data(usersock, 'FILENAME_REQUEST')
            search_results = search_drive(recv_data(usersock))
            if (search_results == -1):
                send_data(usersock, 'File not found')
            else:
                send_data(usersock, search_results)

        elif (option == 'DIRSEARCH'):
            send_data(usersock, 'FILENAME_REQUEST')
            search_results = search_directory(recv_data(usersock))
            if (search_results == -1):
                send_data(usersock, 'File not found')
            else:
                send_data(usersock, search_results)
            
        elif (option == 'DOWNLOAD'):
            send_data(usersock, 'FILENAME_REQUEST')
            send_file_contents(recv_data(usersock), usersock, userinfo)

        elif (option == 'UPLOAD'):
            send_data(usersock, 'FILENAME_REQUEST')
            receive_file_contents(recv_data(usersock), usersock)

        elif (option == 'CLOSE'):
            continue_sentinel = False

        else:
            send_data(usersock, 'Invalid command')
    return

def main():
    while(True):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('', 50001))
        server_socket.listen(8)
        usersock, userinfo = server_socket.accept()

        conn_thread = threading.Thread(target = handle_connection, args = (usersock, userinfo))
        conn_thread.start()
    return

main()
