import socket

def journeyman1(str_list, item):
    filename = str_list[item]
    my_file = open(filename, 'w+')
    for i in range(len(str_list)):
        if i != item:
            my_file.write(str_list[i])
    my_file.close()
    return

def sum_generator(upper_limit):
    i = 0
    while i <= upper_limit:
        yield i
        i += 1

def journeyman2(final_num):
    return sum(sum_generator(final_num))

def journeyman3():
    HOST = '127.0.0.1'
    PORT = 50001
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    my_socket.connect((HOST, PORT))
    received_string = my_socket.recv(1024)
    my_socket.close()
    return received_string

def journeyman4():
    class Person:
        height = 0.0
        weight = 0.0
        hair_color = ''
        eye_color = ''

        def __init__(self, height, weight, hair_color, eye_color):
            self.height = height
            self.weight = weight
            self.hair_color = hair_color
            self.eye_color = eye_color

        def print_info(self):
            print 'Height: %.2f\nWeight: %.2f\nHair color: %s\nEye color: %s' % (self.height, self.weight, self.hair_color, self.eye_color)

    me = Person(1.74, 87.5, 'black', 'brown')
    me.print_info()
    return
